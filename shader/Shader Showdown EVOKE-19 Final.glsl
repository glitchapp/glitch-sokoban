// Winning shader made at EVOKE 2019 Shader Showdown,
// Final round against Flopine / Ohno! ^ X-Men ^ Zen ^ swyng

// The "Shader Showdown" is a demoscene live-coding shader battle competition.
// 2 coders battle for 25 minutes making a shader on stage. No google, no cheat sheets.
// The audience votes for the winner by making noise or by voting on their phone.

// "Love is somebody who will do boring things with you and not later claim to be traumatised by the experience." Christian Fitness - "The brain is not a lever"

vec2 s,v,e=vec2(.00035,-.00035);float t,tt,g,de,cr,f,ff,at,g2;vec3 np,cp,pp,rp,po,no,ld,al;vec4 su=vec4(0);
float bo(vec3 p,vec3 r){p=abs(p)-r;return max(max(p.x,p.y),p.z);}
mat2 r2(float r){return mat2(cos(r),sin(r),-sin(r),cos(r));}
vec2 fb( vec3 p)
{
    p.xz*=r2(0.2-at*0.1);
    vec2 h,t=vec2(bo(abs(p)-vec3(0,0,6),vec3(1,1,2)),5);  
    h=vec2(bo(p,vec3(0.5,0.5,6)),3);  
    h.x=min(bo(abs(p)-vec3(0,0.03,3.56),vec3(10,0.1,0.1)),h.x);  
    t=t.x<h.x?t:h;
    h=vec2(bo(abs(p)-vec3(0,0,9),vec3(0.7,0.7,6)),6);  
    t=t.x<h.x?t:h;
    h=vec2(bo(abs(p)-vec3(0,0.03,3.56),vec3(10,0.1,0.0)),6);
    h.x=min(bo(p,vec3(0.1,0.55,2.5)),h.x);  
    g2+=0.1/(0.1+h.x*h.x*400.);
    t=t.x<h.x?t:h;
    t.x*=0.7;
    return t;
}
vec2 mp( vec3 p)
{
    np=p;
    at=20.-min(length(p-vec3(0,sin(tt)*40.,0))-20.,20.);  
    for(int i=0;i<8;i++){        
        np=abs(np)-vec3(3,0,3);
        if(i>5) np.x=sin(np.x*-.7);
        np.xz*=r2(.785);
        np.xy*=r2(.785);
        np.yz-=2.-at*0.06;
    }
    vec2 h,t=fb(np);    
    h=vec2(2.5*bo(abs(np*.2-vec3(sin(tt)-0.5,0,0.5))-.9,vec3(0.01,10,0.01)),6);
    g+=0.1/(0.1+h.x*h.x*100.);
    t=t.x<h.x?t:h;    
    return t;
}
vec2 tr( vec3 ro,vec3 rd)
{
    vec2 h,t=vec2(.1);
    for(int i=0;i<128;i++){
        h=mp(ro+rd*t.x);
        if(h.x<.0001||t.x>80.)break;
        t.x+=h.x;t.y=h.y;    
    }
    if(t.x>80.)t.y=0.;  
    return t;
}
float noi(vec3 p){
    vec3 f=floor(p),s=vec3(7,157,113);
    p-=f;vec4 h=vec4(0,s.yz,s.y+s.z)+dot(f,s);
    p=p*p*(3.-2.*p);
    h=mix(fract(sin(h)*43758.5),fract(sin(h+s.x)*43758.5),p.x);
    h.xy=mix(h.xz,h.yw,p.y);
    return mix(h.x,h.y,p.z);
}
float cno(vec3 p,float k){
    float f=0.; p.z+=tt*k;
    f+=0.5*noi(p);p=2.1*p;
    f+=0.25*noi(p+1.);p=2.2*p;
    f+=0.125*noi(p+2.);p=2.3*p;
    return f;
}
float cmp( vec3 p)
{  
    float t=0.8*length(abs(p)-vec3(0,abs(sin(tt+1.59))*30.,0))-(8.-sin(p.y*.2-sin(tt)*2.+1.59)*6.);  
    return t;
}
#define a(D) clamp(mp(po+no*D).x/D,0.,1.)
#define s(D) smoothstep(0.,1.,mp(po+ld*D).x/D)
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv=(fragCoord.xy/iResolution.xy-0.5)/vec2(iResolution.y/iResolution.x,1);
    tt=mod(iTime*.8,62.83);
    vec3 ro=vec3(cos(tt*.5)*33.,4.+sin(tt*.5)*30.,sin(tt*.5)*33.),
        cw=normalize(vec3(0)-ro),
        cu=normalize(cross(cw,vec3(cos(tt*0.2-0.5),1,0))),
        cv=normalize(cross(cu,cw)),
        rd=mat3(cu,cv,cw)*normalize(vec3(uv,.4)),co,fo;
    ld=normalize(vec3(0.3,0.5,-0.5));
    co=fo=vec3(0,.2,.3)+rd.y*0.3+0.5*(1.-(length(uv)-0.2));
    s=tr(ro,rd);t=s.x;  
    if(s.y>0.){    
        po=ro+rd*t;no=normalize(e.xyy*mp(po+e.xyy).x+e.yyx*mp(po+e.yyx).x+e.yxy*mp(po+e.yxy).x+e.xxx*mp(po+e.xxx).x);
        al=vec3(1,.5,0);
        if(s.y<5.) al=vec3(0);
        if(s.y>5.) al=vec3(1);
        float dif=max(0.,dot(no,ld)),fr=pow(1.+dot(no,rd),4.),
            spo=exp2(10.*cno(np.zyx,0.)),sp=pow(max(dot(reflect(-ld,no),-rd),0.),spo);
        co=mix(sp+al*(a(.2)*a(.4)+.1)*(dif+s(.4)+s(2.)),fo,min(fr,.5));
        co=mix(co,fo,1.-exp(-.00001*t*t*t));
    }
    cr=cmp(ro)-10.+fract(dot(sin(uv*476.567+uv.yx*785.951),vec2(984.156)));
    for(int i=0;i<70;i++){
        cp=ro+rd*(cr+=1./3.);
        if(su.a>.99||cr>t) break;
        de=clamp(-.5-cmp(cp)+2.*cno(cp,10.),0.,1.);
        su+=vec4(vec3(mix(1.,0.,de)*de),de)*(1.-su.a);
    }    
    co=mix(co,su.xyz*1.5,su.a*.9); //co=mix(co,mix(su.xyz*1.5,fo,1.-exp(-.00001*cr*cr*cr)),su.a*.9);
    fragColor = vec4(pow(co+g*.05+vec3(1,0.5,0)*g2*.3,vec3(.45)),1);
}