#define tau 6.28318
#define numBlobs 15

#define p0 0.5, 0.5, 0.5,  0.5, 0.5, 0.5,  1.0, 1.0, 1.0,  0.0, 0.33, 0.67	
    
// source: http://iquilezles.org/www/articles/palettes/palettes.htm
// cosine based palette, 4 vec3 params
vec3 palette( in float t, in float a0, in float a1, in float a2, in float b0, in float b1, in float b2,
              in float c0, in float c1, in float c2,in float d0, in float d1, in float d2)
{
    return vec3(a0,a1,a2) + vec3(b0,b1,b2)*cos( tau*(vec3(c0,c1,c2)*t+vec3(d0,d1,d2)) );
}


const float radius = .07;
const float edgeWidth = .01;
const int numTrailParticles = 10;
const float trailLength = .3;
const float metaballThreshold = .8;
const float metaballRadius = .00325;
const float speedMultiplier = 1.1;
vec2 getPos(float t, float aspect) {
    float tx = t / aspect;
    vec2 p = vec2(sin(2.2 * tx) + cos(1.4 * tx), cos(.3 * t) + sin(-1.9 * t));
    p.y *= 0.2;
    p.x *= 0.4;
    return p;
}
float metaballFunc(vec2 pos, vec2 uv, float radius)
{
    return radius / length(uv - pos);
}
void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = fragCoord.xy / iResolution.xy - vec2(.5);
    uv.x *= iResolution.x / iResolution.y;
    vec3 color = vec3(.0, 0., .0);
    vec3 colorFinal = vec3(0.);
    float finalFactor = 0.;
    float aspect = iResolution.x / iResolution.y;
    float mb = 0.;
    float[numBlobs] closeness;
    float tot = 0.;
    
    for (int i = 0; i < numBlobs; i++)
    {
        float minDistance = 1.0;
        float colorMul = 1.0;
        for (int j = 0; j < numTrailParticles; j++) {
            float pct = float(j) / float(numTrailParticles);
            vec2 pos = getPos(iTime * speedMultiplier + float(i) * 1.5 - pct * trailLength, aspect);
            float segmentLength = length(pos - uv);
            minDistance = min(minDistance, segmentLength + pct * (radius + edgeWidth));
            mb += metaballFunc(pos, uv, metaballRadius * pct);
        }
        finalFactor += smoothstep(radius + edgeWidth, radius, minDistance);
    	closeness[i] = pow(1. / minDistance, 3.);
    	tot += closeness[i];
    }
    
    for (int i = 0; i < numBlobs; i++) {
    	color += (closeness[i] / tot) * palette(float(i) / 2., p0);
    }
        
    finalFactor += smoothstep(mb + .03, mb, metaballThreshold);
    
    fragColor = vec4(min(finalFactor, 1.) * color * 1.2, 1.0);
}
