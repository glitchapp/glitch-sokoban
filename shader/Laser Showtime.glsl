#line 2
//////////////////////////////////////////////
//
// Laser show by Timo Kinnunen 2017
//
// Use mouse to override the default cycling
// pattern.
//
// Based on line AO by snake5 
// @ https://www.shadertoy.com/view/XsjyRy
//
// Licensed under default Shadertoy license.
//
//////////////////////////////////////////////

const float bidpScale = -0.25; //0.25;
const float bidpAdd = 0.75;
const float falloffDist = 100.0;
const float falloffCurve = 0.2; //0.4;

float lineAOv2(vec2 pos, vec2 line0, vec2 line1) {
	vec2 dir = normalize(line1- line0);
	vec2 rt = vec2(-dir.y,dir.x);
	float tp0 = dot(rt,line0);
	float tpx = dot(rt,pos);
	float bidp = dot(normalize(line0- pos),normalize(line1- pos))* bidpScale+ bidpAdd;
	float distf = clamp(abs(tp0- tpx)/ falloffDist,0.0,1.0);
	return 1.0- (1.0- bidp)* (1.0- pow(distf,falloffCurve));
}
float lineAOx2(vec2 pos, vec2 line0, vec2 line1, vec2 line2) {
	return lineAOv2(pos,line0,line1)* lineAOv2(pos,line1,line2);
}
float vee(vec2 pos, vec2 line0, vec2 line1) {
	return lineAOx2(pos,line0,line1,vec2(line1.x* 2.- line0.x,line0.y));
}
vec3 vee3(vec2 uv, vec3 x1, vec3 y1, vec3 x2, vec3 y2) {
	return vec3(
        vee(uv,vec2(x1.r,y1.r),vec2(x2.r,y2.r)),
        vee(uv,vec2(x1.g,y1.g),vec2(x2.g,y2.g)),
        vee(uv,vec2(x1.b,y1.b),vec2(x2.b,y2.b)));
}
void mainImage(out vec4 fragColor, vec2 fragCoord) {
	const float duration1 = 15.;
	const float duration2 = 30.* duration1;
	float beg = (iMouse.y> 15.)? (iMouse.y- 15.)/ (iResolution.y- 15.): abs(2.* fract(iTime/ duration1)- 1.);
	float end = (iMouse.x> 15.)? (iMouse.x- 15.)/ (iResolution.x- 15.): abs(2.* fract(iTime/ duration2)- 1.);
	beg = floor(60.* beg- 30.);
	end = beg+ ceil(30.* end);

	vec2 uv = fragCoord.xy/ iResolution.xy* vec2(1920,1200);
	const float midpoint = 300.0;
	float fade = smoothstep(midpoint- 2.,midpoint+ 2.,uv.y);
	uv.y = abs(uv.y- midpoint)+ midpoint;

	vec3 s = vec3(0.9);
	for(float k = 0.; k< 30.; k++) {
		float i = beg+ k;
		if(i>= end) break;
		i = mod(i+ 15.,30.0)- 15.;
		vec3 ys = sin(iTime* .5+ vec3(0,.125,.25));
		vec3 y1 = midpoint+ (25.+ 25.* ys* 6.)* i;
		vec3 y2 = midpoint+ (20.+ 20.* ys* 5.)* i;
		vec3 x1 = vec3(100,850,1600)+ 10.* i;
		vec3 x2 = vec3(200,955,1710);
		s *= vee3(uv,x1,y1,x2,y2);
	}
	float r = s.r,g = s.g,b = s.b;

	vec3 color = vec3(g+ b- g* b,r+ b- r* b,r+ g- r* g)- r* g* b;
	color = pow(color,vec3(5.- 2.* fade));
	color *= 1.0+ 0.5* fade;
	fragColor = vec4(color,1);
}
