const vec3 beamCol = vec3(.5,.2,.1);
const vec3 glowCol = vec3(0.2,.5,0.2);

const int pointCount = 8;
const vec3 points[pointCount] = vec3[pointCount](
    vec3(.2,+.2,+.2),
    vec3(.2,+.2,-.2),
    vec3(.2,-.2,-.2),
    vec3(.2,-.2,+.2),
    
    vec3(-.2,+.2,+.2),
    vec3(-.2,+.2,-.2),
    vec3(-.2,-.2,-.2),
    vec3(-.2,-.2,+.2)
);

const int lineCount = 12;
const ivec2 lines[lineCount] = ivec2[lineCount](
    ivec2(0,1),
    ivec2(1,2),
    ivec2(2,3),
    ivec2(3,0),
    
    ivec2(4,5),
    ivec2(5,6),
    ivec2(6,7),
    ivec2(7,4),
    
    ivec2(0,4),
    ivec2(1,5),
    ivec2(2,6),
    ivec2(3,7)
    );

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 dLine(vec2 p, vec2 start, vec2 end,float thickness)
{
    
    vec2 dir = normalize(end-start);
    float l = length(start-end);
    vec2 ndir = vec2(dir.y,-dir.x);
    float dLine = abs(dot(start-p,ndir));
    float dFuss = dot(p-start,dir);
    if(dFuss<0.)
        dLine = length(p-start);
    else if(dFuss > l)
        dLine = length(p-(start+l*dir));
        
    thickness+=(rand(p*iTime*dFuss)-0.5)*0.001;
        
    vec3 ret=smoothstep(3.*thickness,thickness*1.1,dLine)*beamCol;
    if(dLine>thickness)
        ret+=smoothstep(8.*thickness,thickness,dLine)*glowCol;
    return ret;
}


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = (fragCoord-iResolution.xy/2.)/max(iResolution.x,iResolution.y);

    vec3 up = vec3(0.,0.,1.);
    float camDist = (sin(iTime*.2)+2.);
    vec3 camPos = vec3(camDist*sin(iTime),camDist*cos(iTime),0.8*sin(iTime*0.8));
    vec3 camDir = normalize(-camPos);
    vec3 camX = cross(camDir,up);
    vec3 camY = cross(camDir,camX);
    
    mat3 camFrame = mat3(camX,camY,camDir);
    
    vec3 projected[pointCount];
    for(int n=0;n<pointCount;n++)
    {
		projected[n] = (points[n] - camPos)*camFrame;
        projected[n].xy /= projected[n].z;
    }

    fragColor = vec4(0.,0.,0.,1.0);
    for(int n=0;n<lineCount;n++)
    {
    	fragColor += vec4(dLine(uv,projected[lines[n].x].xy,projected[lines[n].y].xy,0.003/projected[lines[n].y].z),1.);
    }
}