const float size = .3;
const float thickness = .05;
const float iterations = 15.;

mat2 rotate(float rads)
{
    return mat2(cos(rads), sin(rads), -sin(rads), cos(rads));
}

float getDist(vec2 uv)
{
    vec2 coord = rotate(sin(iTime * .65) * 2.35) * abs(fract(uv) - .5);
    return max(coord.x, coord.y);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 uv = 1.75 * (2. * fragCoord.xy - iResolution.xy) / iResolution.y;
    vec2 stepAmt = uv * .005;
    float colAmt = 0.;
        
    for (float i = 0.; i < iterations; i++)
    {
        uv += stepAmt * i;
        float maxDist = getDist(uv);
        colAmt += (smoothstep(size, size - .05, maxDist)
        * smoothstep(size - thickness, size - (thickness - .05), maxDist));
    }
        
    fragColor = vec4(colAmt + (1. - step(0.2, colAmt)) * .1, 0., 0., 1.);
}