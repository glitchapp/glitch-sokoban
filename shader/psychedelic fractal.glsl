#define res iResolution.xy
#define tau 6.283
#define eps .01

float sdf(vec2 p) {
    float dist = length(p) - .5;
    dist = min(dist, p.y + 1.);
    dist = min(dist, 1. - p.y);
    dist = min(dist, p.x + res.x/res.y);
    dist = min(dist, res.x/res.y - p.x);
    return dist;
}

void mainImage(out vec4 col, in vec2 coord) {
    vec2 pos = (coord * 2. - res) / res.y;
    for(int i = 0; i < 32; i++) {
        float dist = sdf(pos)*2.;
        if(abs(dist) < .01) break;
        pos += dist * vec2(sin((vec2((i+1)*(i+1))*iTime*.001 + vec2(0,.25)) * tau));
    }
    col = vec4(normalize(pos) * .5 + .5, 0, 1);
}
