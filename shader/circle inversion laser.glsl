//parent: https://www.shadertoy.com/view/lslfRl

#define camLens 6.
#define frame(u) camLens*(u-.5*iResolution.xy)/iResolution.y 

//#define fractit

//#define mixRatio sin(iTime)*.5+.5
//#define mixRatio 0.
#define mixRatio 1.
//#define mixRatio -1.
//#define mixRatio 2.

//different types of sphere inversios, some are "one way" only.
//#define invert(a) a.x=max(a.x,1./a.x)
//#define invert(a) a.x=min(a.x,1./a.x)
#define invert(a) a.x=1./a.x



/*
the idea behind this was my thesis, that turned to be false:
because i treated inersion as if it is refraction.

anyways:
if(origin is outside of circle)
...the CUVED lines intersed on the circle.
...This does not help me now, but looks neat anyways.

but i got a nice sandbox for textures ou if it.

This projects onto a UNIT circle, with origin vee2(0,0) and radius float 1.;
You want to calculate the NEAREST inttersection of a line and a circle (or sphere...)
The classic solution branches a lot, and it swivels near and far intersection.
The classic solution is all not too pretty, I despise branhes.
there is a simple sub-case: if(rayOrigin is INSIDE the circle)
- this ray2circle intersection case has NULL branches, always exactly one intersection.
- a circle inversion projects the if(rayOrigin OUTSIDE OF In the circle)
- ... into the simpler if(rayOrigin is INSIDE the circle) case.
- ... and they result in the same rayToCircle intersection point.
Circle inversion enables to use the same subroutine for 3 cases,wapped in one conditional:
if (rayIntersectsSphere)==(gSRx()) (function exists below)
- if(length(origin)< 1.)                return rayinsideSphere()//origin is inside circle
- if(length(origin)> 1.) circleInvert() return rayinsideSphere()//origin is outside circle
- if(length(origin)==1.)                return origin;//seems like a shortcut
This looks to me like smart clean reusing code.
A caveat is the atan2() of circleInvert().
*/


//ray [o,d]origin.,direction starts inside sphere [r]radios 
//return intersection of ray from within sphere (is less brancing)
vec3 rayinsideSphere(float r,vec3 o,vec3 d){
 if(d==vec3(0))return vec3(0);
 float a=dot(d,d);
 float b=2.*dot(d,o);
 float c=dot(o,o)-r*r;
 float e=b*b-4.*a*c;
 //if(disc<0.)return vec3(0);
 e=sqrt(e);
 float q;
 if(b<0.)q=(-b-e)*.5;
 else    q=(-b+e)*.5;
 vec2 t=vec2(q/a,c/q);//t stores 2 intersection distances.
 if(t.x>t.y)t=t.yx;//swivel to make t.x closest intersection.
 t.x=mix(t.x,t.y,step(t.x,0.));//if(t.x>0.)t.x=t.x;else t.x=t.y;
 return o+(t.x*d);}//second life wiki geometric
/*
//above and below make me think about sphere inverting below, 
//to become the above, for the case where intersection exists.
//return intersection of [o]RayOrigin  [d]RayDirection and circle [r]Radius 
//this branches too often
vec3 gSRxX(float r, vec3 o, vec3 d){
 if(r>length(o))return rayinsideSphere(r,o,d);
 //float t; //o -= Sp;
 if(d==vec3(0))return vec3(0);
 float a=dot(d,d);
 float b=2.*dot(d,o);
 float c=dot(o,o)-r*r;
 float disc=b*b-4.*a*c;
 if(disc<0.)return vec3(0);
 disc=sqrt(disc);
 float q; 
 if(b<0.)q=(-b-disc)*.5;
 else    q=(-b+disc)*.5;
 vec2 t=vec2(q/a,c/q);//t stores 2 intersection distances.
 if(t.x>t.y)t=t.yx;//swivel to make t.x closest intersection.
 //there seems to be some sign nonsense going on here.
 //...because this unifies the cases of originInsideCircle and OriginOutsideCircle
 if(t.y<0.)return vec3(0);//both negative, sphere is behind camera.
 t.x=mix(t.x,t.y,step(t.x,0.));//if(t.x>0.)t.x=t.x;else t.x=t.y;
 return o+(t.x*d);}//second life wiki geometric
*/

//return if(ray intersects sphere) [r]radius [o]RayOrigin [d]RayDirection
bool gSRx(float r, vec3 o, vec3 d){
 //if(d==vec2(0)) return false;
 float b=2.*dot(d,o);
 if(b*b-4.*dot(d,d)*(dot(o,o)-r*r)<0.)return false;return true;}

#define tau acos(-1.)*2.
#define Phi (sqrt(5.)*.5+.5)
#define phi (sqrt(5.)*.5-.5)


//return polar of carthesian
vec2 c2p(vec2 u){return vec2(length(u),atan(u.y,u.x));}
//return carthesian of polar
vec2 p2c(vec2 u){return vec2(u.x*cos(u.y),u.x*sin(u.y));}

//mirror around half rotated axis.
#define r2(r) mat2(sin(r+vec4(1,0,0,-1)*asin(1.)))
//return distance to ray_from_(0,0)_to_(0,infinity)="Infinite Line Segment"
float ils(vec2 u){return mix(length(u),abs(u.y),step(u.x,0.));}
//return distance to ray from [a] trough [b]
float ils2(vec2 b,vec2 a){vec2 d=b-a;return ils(a*r2(atan(d.y,d.x)));}

void mainImage( out vec4 O, in vec2 U ){
	vec2 u=frame(U);
    vec2 m=frame(iMouse.xy);
    vec2 n=frame(iMouse.zw);
    if(length(m-n)<.1)n.x+=1.;//direction is never vec2(0)
    if(iMouse.z<=0.){
     m=vec2(0.,sin(iTime))*Phi;
     n=vec2(cos(iTime),0.)*phi;
    m*=2.;
    n*=2.;
    }
    

    u.x+=sin(iTime);
    u.y+=cos(iTime);    
    m.x+=sin(iTime);
    m.y+=cos(iTime);
    n.x+=sin(iTime);
    n.y+=cos(iTime);
    
    float w=ils2(m-u,n-u);//THIN white line happens before transforming to polar.   
    float blur = 0.025;
    float l=smoothstep(-blur,blur,abs(1.-length(u))-blur);
    //indicate sphere boundary by making points near (l=0) darker. 
    u=c2p(u);
    m=c2p(m);
    n=c2p(n);
    vec2 a=u;if(a.x<1.)a.x=1./a.x;//if(inside unit circle)  do circle inversion
    vec2 b=u;if(b.x>1.)b.x=1./b.x;//if(outside unit circle) do circle inversion
    u=mix(a,b,mixRatio);
    invert(u);
    //invert(m);
    //invert(n);
    w=min(w,ils2(m-u,n-u));
    
    u=p2c(u);
    m=p2c(m);
    n=p2c(n);   
    //w=min(w,ils2(m-u,n-u));
    
    float g=ils2(m-u,n-u);
    u.x-=cos(iTime);
    u.y-=sin(iTime);
    m.x-=cos(iTime);
    m.y-=sin(iTime);
    n.x-=cos(iTime);
    n.y-=sin(iTime);
   
    float c=length(m-u);
    float d=length(n-u);
    g=min(g*3.,min(c,d));
    
    u=fract(u*camLens);
    u.x*=.5;
    #ifdef fractit
    g=fract(g*5.);
    #else 
    g=smoothstep(blur,-blur,g-.1);
    #endif   
    w=smoothstep(blur,-blur,w-.001);
    vec3 e=u.x*vec3(1,.5,0)+u.y*vec3(.5,0,1)+g*vec3(0,1,.5)+w*vec3(1,1,1);
    e=min(e,vec3(l));
    //BlackredBluePurple->BlackYellowCyanWhite == more contrast on lcd or oled.
    O = vec4(e,1);
}

//doto, in3d, via
//https://en.wikipedia.org/wiki/List_of_common_coordinate_transformations