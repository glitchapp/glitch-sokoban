--[[
 Copyright (C) Glitchapp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


The license text can be found in GPL-2.0.txt.

Description / resume of the functionality of each function written with assistance from ChatGPT

--]]

--[[
The code provided is written in Lua and is meant to be executed within the Love2D game engine framework. It includes the following functions:

    love.load(): This function is called once at the beginning of the game and is used for loading game assets and initializing variables. The function includes the following:
--]]

game = {}
game.w, game.h, game.flags = love.window.getMode()
game.max_fps = game.flags.refreshrate
game.min_dt = 1/game.max_fps
game.next_time = love.timer.getTime()
--game.icon = love.image.newImageData("data/images/icon.png")
game.runtime = os.time()
game.ticks = 0
game.utick_time = 0
game.dtick_time = 0

	 LastOrientation="portrait"
 MobileOrientation="portrait"
 
 width, height = love.graphics.getDimensions()
 --print(width)

-- This function is called once at the beginning of the game
function love.load()
require ('TopMenu')

require ('shader/NormalsAndNoiseAndShadowsandEnvmap')

touchControls = require("touchpressed")
touchControls = require("touchControls/touchControls")
touchControls:init()

  touchControls = require("touchControls/touchControls")
 touchKeyboard = require ("touchControls/touchKeyboard")
 

 
require ("touchControls/Menu/About")
require ("touchControls/Menu/Buttons")
require ("touchControls/Menu/DrawButton")
 touchControls:init()
  
  -- Load external libraries and scripts
  Object = require "classic"			-- Object-oriented programming library
  --menuengine = require "menuengine"		-- Menu engine library

  require 'levels'						-- Script containing level data
  
  -- Load input handling scripts
  --require ('gamepadpressed')			-- Script to handle gamepad input
  --require ('keypressed')           		-- Script to handle keyboard input
  --require ('touchpressed')				-- Script to handle touch input
  --touchinterfaceison=true
  --require ('mousepressed')				-- Script to handle mouse input
  
  
  --require ('help')						-- Script to display help information
  --require ('credits')					-- Script to display credits

    -- Load debugging scripts
  --require('findboxes')  				-- Script to print position of boxes for debug
  --require('findplayer')					-- Script to print position of player for debug
  
  -- Load game assets
  require('loadassets')					-- Script to load game assets
loadassets() 							-- Call the script to load assets

  -- Initialize variables
  gamestate="game"						-- Set the initial game state
  levelpreview=0						-- Set the initial level preview
  myres="800x600"						-- Set the initial resolution
  cameraon=false						-- Set the initial camera state
  helpison=false						-- Set the initial help dialog state
  creditsison=false						-- Set the initial credits dialog state
  --shaderselect="moonshine"				-- Set the initial shader effect
     
  -- Load external scripts
  require"dialogs"						-- Script to write dialogs
  --require"startmenu"					-- Script to start menu
    --startmenu.load()
  require"displayalllevels"				-- Script to display all levels
  
  --shadertoy files
  --require"maplogicshadertoy"			-- Script to handle shadertoy effects
  --require"shadertoy1"					-- Script to assign shadertoy effects
  --require"shadertoyconfig"			--It assigns shadertoy effects
  --map1stoy = maplogicshadertoy()		-- Create an instance of the maplogicshadertoy object
  
  --moonshine files
  --require"moonshine"
  require"maplogic"						-- Script to handle maps
  require"shadersconfig"				-- Script to assign moonshine effects
  map1=maplogic()						-- Create an instance of the maplogic object
  
  --optionsmenu = require ('game/optionsmenu')			-- Script to load level selection menu
  --optionsload()											-- Call the function to load options
  
  musicplayer = require ('game/musicplayer')			-- Script to handle music playback
  musicload()											-- Call the function to load music
  
  --Instances of several objects are created, including
  dialogs1=dialogs()			-- Create an instance of the dialogs object
  --map1 = maplogic()				--instance of object maplogic
  
  --mapall = displayalllevels(1)	 -- Create an instance of the displayalllevels object
  --menu1 = startmenu()			-- Create an instance of the startmenu object

  
end
mouseHeld = false
  local timeAccumulator = 0
local updateInterval = 0.1  -- Adjust this value for your desired update speed
timeInt = 0

-- This function is called every frame to update the game state
function love.update(dt)

--checkOrientation()
--touchControls_update(dt)

	   timeAccumulator = timeAccumulator + dt
    if timeAccumulator >= updateInterval then
        timeInt = timeInt + 1  -- Increment integer time
        timeAccumulator = timeAccumulator - updateInterval  -- Reset accumulator
    end
    
    vectorNormalsNoiseShadowEnvmapShader:send("Time", timeInt * 10)  -- Send time to shader

  
		map1:update(dt)				-- Update the moonshine effect map
  
	touchTranslation(dt) 
end


--love.draw(): This function is called every frame and is used for rendering graphics to the screen.
function love.draw()
		
		if MobileOrientation == "portrait" then
		--love.graphics.scale( 3 )
		--love.graphics.rotate( 0 )
		--love.graphics.translate(0,0)
	elseif MobileOrientation == "landscape" then
		--love.graphics.scale( 2 )
		love.graphics.rotate( 1.57 )
		love.graphics.translate(200,-900)
	end
		
	  if gamestate=="game" then 					-- This line checks if the game state is "game" and the shader selected is "moonshine". 
			--love.graphics.setShader
			 map1:draw()
			 dialogs1:draw()  				-- If so, it sets the shader to the default shader and then calls the draw() function on the map1 and dialogs1 objects. This line is responsible for drawing the game map and any dialogs that might appear during the game.
  elseif gamestate=="game" and shaderselect=="shadertoy" then					-- This line checks if the game state is "game" and the shader selected is "shadertoy".
			map1stoy:draw()														-- If so, it calls the draw() function on the map1stoy object. This line is responsible for drawing the game map with a shadertoy effect applied.
  elseif gamestate=="preview" then					-- This line checks if the game state is "preview". 
			love.graphics.clear() --mapall:draw() 	-- If so, it clears the screen and then calls the draw() function on the mapall object. This line is responsible for drawing the level preview screen.
			displayalllevels:draw()
			love.graphics.print("Move the thumbstick to navigate the levels", 25,30,0,0.2)
  elseif gamestate=="menu"	then					-- This line checks if the game state is "menu".
			startmenu.draw() 							-- If so, it calls the draw() function on the menu1 object. This line is responsible for drawing the game menu.
  elseif gamestate=="options" then					--This line checks if the game state is "options".
			--love.graphics.setShader optionsmenudraw(dt) --If so, it sets the shader to the default shader and then calls the optionsmenudraw() function with the dt parameter. This line is responsible for drawing the options menu.
  elseif gamestate=="music" then					--This line checks if the game state is "music".
			--love.graphics.setShader
			musicdraw(dt)	-- If so, it sets the shader to the default shader and then calls the musicdraw() function with the dt parameter. This line is responsible for drawing the music menu.
  end		
  
  touchControls:draw()
  if menuVisible==true then
			revealMenu(x, y, button, istouch, presses)
		end
end
    
    function checkOrientation()
	local desktopWidth, desktopHeight = love.window.getDesktopDimensions()
	--print(desktopWidth)
	--MobileOrientation="landscape"
		if desktopWidth<1100 then MobileOrientation="portrait"
			if LastOrientation==MobileOrientation then 
				
			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / 2
				scale_y = love.graphics.getHeight() / 2
				--love.window.setMode(2340, 1080, {resizable=true, borderless=false})
				touchControls:createArcadeButtons()
				
			end
	elseif desktopWidth>1100  then MobileOrientation="landscape"
			if LastOrientation==MobileOrientation then 
				
			else	
				LastOrientation=MobileOrientation
				scale_x = love.graphics.getWidth() / 2
				scale_y = love.graphics.getHeight() / 2
				--love.window.setMode(1080, 2340, {resizable=true, borderless=false})
				touchControls:createArcadeButtons()
				
			end
	end
end


