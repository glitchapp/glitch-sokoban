mousepressed = Object:extend()

function love.mousepressed() 
  
   local mousex,mousey = love.mouse.getPosition()
  
        local playerX   -- Player position and
        local playerY   -- Adjacent position in the direction of the arrow key pressed
        
        for testY, row in ipairs(level) do
            for testX, cell in ipairs(row) do
                if cell == player or cell == playerOnStorage then
                    playerX = testX
                    playerY = testY
                end
            end
        end

        local dx = 0
        local dy = 0

--left coordinates:x 540 -630 y 410 505
        if love.mouse.isDown(1) and mousex>540 and mousex<630 and mousey>427 and mousey<495 then --left
            dx = -1
        --right coordinates: y 694 770 y 427 505
        elseif mousex>690 and mousex<770 and mousey>427 and mousey<495 then --right
            dx = 1
        --down coordinates: x 621 693 y 610 710
        elseif mousex>631 and mousex<689  and mousey>350 and mousey<440 then--down
            dy = -1
        --up coordinates: x 621 693 y 470 570
        elseif mousex>631 and mousex<689 and mousey>470 and mousey<570 then --up
            dy = 1
         --trigger virtual gamepad x 773 795 y 533 582
        elseif mousex>735 and mousex<795 and mousey>533 and mousey<582 then --up
            if touchinterfaceison==true then touchinterfaceison=false
              else touchinterfaceison=true
            end
	
        end
		
        

        local current = level[playerY][playerX]
        local adjacent = level[playerY + dy][playerX + dx]
        local adjacent = level[playerY + dy][playerX + dx]
        local beyond
        --Pushing box on to empty location
        if level[playerY + dy + dy] then
            beyond = level[playerY + dy + dy][playerX + dx + dx]
        end

        local nextAdjacent = {
            [empty] = player,
            [storage] = playerOnStorage,
        }

        local nextCurrent = {
            [player] = empty,
            [playerOnStorage] = storage,
        }

        local nextBeyond = {
            [empty] = box,
            [storage] = boxOnStorage,
        }

        local nextAdjacentPush = {
            [box] = player,
            [boxOnStorage] = playerOnStorage,
        }

        if nextAdjacent[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacent[adjacent]

        elseif nextBeyond[beyond] and nextAdjacentPush[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacentPush[adjacent]
            level[playerY + dy + dy][playerX + dx + dx] = nextBeyond[beyond]
        end

        local complete = true

        for y, row in ipairs(level) do
            for x, cell in ipairs(row) do
                if cell == box then
                    complete = false
                end
            end
        end

        if complete then
            currentLevel = currentLevel + 1
            if currentLevel > #levels then
                currentLevel = 1
            end
            loadLevel()
        end
    end
  
