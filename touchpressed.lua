
-- Define delay interval (in seconds)
local moveDelay = 0.5 -- half a second
local timeSinceLastMove = moveDelay -- Initialize to delay to allow immediate first move


function touchTranslation(dt) 
  
      -- Update elapsed time since last move
    timeSinceLastMove = timeSinceLastMove + dt
	--print(timeSinceLastMove)
    -- Check if the delay interval has passed
    if timeSinceLastMove < moveDelay then
        return  -- Not enough time has passed, exit the function
    end
  
         local playerX   -- Player position and
        local playerY   -- Adjacent position in the direction of the arrow key pressed
        
        for testY, row in ipairs(level) do
            for testX, cell in ipairs(row) do
                if cell == player or cell == playerOnStorage then
                    playerX = testX
                    playerY = testY
                end
            end
        end

        local dx = 0
        local dy = 0
        --print(touchControls.leftxaxis)
        --print(touchControls.leftyaxis)
        
        --left coordinates:x 540 -630 y 410 505
        --if love.touch.pressed() and touchx>540 and touchx<630 and touchy>427 and touchy<495 then --left
        if touchControls.leftxaxis<-350 then
			dx = -1
        end
            
        --right coordinates: y 694 770 y 427 505
        --elseif touchx>690 and touchx<770 and touchy>427 and touchy<495 then --right
        if touchControls.leftxaxis>350 then
            dx = 1
        end
        --down coordinates: x 621 693 y 610 710
        --elseif touchx>631 and touchx<689  and touchy>350 and touchy<440 then--down
        if touchControls.leftyaxis<-350 then
            dy = -1
        end
        --up coordinates: x 621 693 y 470 570
        --elseif touchx>631 and touchx<689 and touchy>470 and touchy<570 then --up
        if touchControls.leftyaxis>350 then
            dy = 1
        end

        local current = level[playerY][playerX]
        local adjacent = level[playerY + dy][playerX + dx]
        local beyond
        --Pushing box on to empty location
        if level[playerY + dy + dy] then
            beyond = level[playerY + dy + dy][playerX + dx + dx]
        end

        local nextAdjacent = {
            [empty] = player,
            [storage] = playerOnStorage,
        }

        local nextCurrent = {
            [player] = empty,
            [playerOnStorage] = storage,
        }

        local nextBeyond = {
            [empty] = box,
            [storage] = boxOnStorage,
        }

        local nextAdjacentPush = {
            [box] = player,
            [boxOnStorage] = playerOnStorage,
        }

        if nextAdjacent[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacent[adjacent]

        elseif nextBeyond[beyond] and nextAdjacentPush[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacentPush[adjacent]
            level[playerY + dy + dy][playerX + dx + dx] = nextBeyond[beyond]
          
        end

        local complete = true

        for y, row in ipairs(level) do
            for x, cell in ipairs(row) do
                if cell == box then
                    complete = false
                end
            end
        end

        if complete then
            currentLevel = currentLevel + 1
            if currentLevel > #levels then
                currentLevel = 1
            end
            loadLevel()
        end
        
timeSinceLastMove = 0
end
