startmenu = Object:extend()

--menuengine = require "menuengine"
menuengine.stop_on_nil_functions = true

local text = "Nothing was selected."

-- Mainmenu
local mainmenu

 function startmenu:new(x,y)
        startmenu.load()

    end


    function sleep(s)
        local ntime = os.clock() + s/10
        repeat until os.clock() > ntime
      end
      
local function mainmenu_finish(entrypoint)
		if entrypoint == 1 then
			love.graphics.clear()
			text = "Start game selected"
			love.graphics.clear()
			menuengine.disable()
			--[[gamestate="game"
		elseif entrypoint == 2 then
			text = "Resolution 800x600 selected!"
			love.window.setMode(800,600)
			love.graphics.setBackgroundColor(0, 0, 0)
			myres="800x600"
			loadLevel() 
		elseif entrypoint == 3 then
			text = "Resolution 1920x1080 selected"
			love.window.setMode(1920,1080)
			myres="1920x1080"
			love.graphics.setBackgroundColor(0, 0, 0)
			loadLevel() 
		elseif entrypoint == 4 then
			text = "Quit Game was selected!"
			os.exit()
			--]]
		end
	
end

function startmenu.load()
    
    love.window.setMode(800,600)
    love.graphics.setFont(love.graphics.newFont(20))
	
    mainmenu = menuengine.new(300,300)
    --mainmenu:addEntry("press p to continue", mainmenu_finish, 1)  -- call "mainmenu_finish", args = "1"
    --[[mainmenu:addSep()
    mainmenu:addEntry("Resolution: 800x600", mainmenu_finish, 2)  -- call "mainmenu_finish", args = "1"
	mainmenu:addEntry("Resolution: 1980x1080", mainmenu_finish, 3)  -- call "mainmenu_finish", args = "1"mainmenu:addSep()
	mainmenu:addSep()
    mainmenu:addEntry("Quit Game", mainmenu_finish, 4)  -- call "mainmenu_finish", args = "2"
    --]]
    
end



function startmenu.update(dt)
    if gamestate=="menu" then mainmenu:update() end

end


function startmenu.draw()
love.graphics.setShader(shader)
mainmenu:draw()
	if gamestate=="menu" then 
		love.graphics.setFont(love.graphics.newFont(40))
        love.graphics.print("Glitch Sokoban v0.7",180,150)	
        love.graphics.setFont(love.graphics.newFont(24))
        love.graphics.print("Pull down to choose an option",240,250)	
        love.graphics.setFont(love.graphics.newFont(12))
    end

end



function love.mousemoved(x, y, dx, dy, istouch)
    menuengine.mousemoved(x, y)
end
