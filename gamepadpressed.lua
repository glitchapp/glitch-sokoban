gamepadpressed = Object:extend()

	--Get table of all connected Joysticks:
				local joysticks = love.joystick.getJoysticks()
			--Pick first one:
				local joystick = joysticks[1]

--require('findboxes')  --print position of boxes for debug
--require('findplayer') --print position of player for debug

function love.gamepadpressed(joystick,button)

findboxes() --print position of boxes for debug
   findplayer() --print position of boxes for debug

		local playerX   -- Player position and
        local playerY   -- Adjacent position in the direction of the arrow key pressed
        --findbox()
        for testY, row in ipairs(level) do
            for testX, cell in ipairs(row) do
                if cell == player or cell == playerOnStorage then
                    playerX = testX
                    playerY = testY
                end
            end
        end
			--find cell type below box

        -- Temporary
       -- print("player x:" ..(playerX),"y:".. (playerY))
        local dx = 0
        local dy = 0
        if joystick:isGamepadDown("dpleft") then
            dx = -1
        elseif joystick:isGamepadDown("dpright") then
            dx = 1
        elseif  joystick:isGamepadDown("dpup") then
            dy = -1
        elseif joystick:isGamepadDown("dpdown") then
            dy = 1
		end

        local current = level[playerY][playerX]
        local adjacent = level[playerY + dy][playerX + dx]
        local beyond
        --Pushing box on to empty location
        if level[playerY + dy + dy] then
            beyond = level[playerY + dy + dy][playerX + dx + dx]
        end

        local nextAdjacent = {
            [empty] = player,
            [storage] = playerOnStorage,
        }

        local nextCurrent = {
            [player] = empty,
            [playerOnStorage] = storage,
        }

        local nextBeyond = {
            [empty] = box,
            [storage] = boxOnStorage,
        }

        local nextAdjacentPush = {
            [box] = player,
            [boxOnStorage] = playerOnStorage,
        }

        if nextAdjacent[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacent[adjacent]

        elseif nextBeyond[beyond] and nextAdjacentPush[adjacent] then
            level[playerY][playerX] = nextCurrent[current]
            level[playerY + dy][playerX + dx] = nextAdjacentPush[adjacent]
            level[playerY + dy + dy][playerX + dx + dx] = nextBeyond[beyond]
        end

        local complete = true

        for y, row in ipairs(level) do
            for x, cell in ipairs(row) do
                if cell == box then
                    complete = false
                end
            end
        end

        if complete then
            currentLevel = currentLevel + 1
            if currentLevel > #levels then
                currentLevel = 1
            end
            loadLevel()
        end
end
		
