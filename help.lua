help= Object:extend(loadLevel)

help = [[
Controls:
Move: Arrows, gamepad dpad, virtual touchpad (with mouse or touching)                                

1: go back to the previous level
2: skip one level
3: switch resolution 1080p / 800x600
4: switch between shader sets. Available: Moonshine / Shadertoy
5 to skip 5 levels

R to repeat level			c: camera follows player on/off

h: show / hide help			t: virtual touch pad on/off
p: Pause / continue game	n: switch on / off level preview

q: quit
]]


