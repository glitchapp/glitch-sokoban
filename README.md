# glitchsokoban

A fork of tutorials for Löwe https://simplegametutorials.github.io/love/sokoban/

100 levels and different combinations of shaders, colours, effects and tilesets.

Ingame Music player and Soundtrack from "OIL" (https://soundcloud.com/oyorama) (https://oilofficial.bandcamp.com)

<img src="screenshots/newsc1.avif" width=50% height=50%>
<img src="screenshots/newsc2.avif" width=50% height=50%>
<img src="screenshots/newsc3.avif" width=50% height=50%>

Installation procedure:

Download löve from https://love2d.org/ and install it.

Download https://github.com/idbrii/love-moonshine and unzip it inside the game's folder with the name "moonshine"

Run from terminal or cmd from the parent folder with "love  glitchsokoban" or zip the content of the game's folder and rename .zip to .love