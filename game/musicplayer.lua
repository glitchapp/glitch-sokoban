
function musicload()

author="none"
currenttrackname=""
currenttrackdesc=""
bcspace=80
trackspositionx=300
trackspositiony=200

author="none"
currenttrackname=""
currenttrackdesc=""

oilt=[[oil]]
oilt2=[[https://soundcloud.com/oilrama]]
oilt3=[[https://oilofficial.bandcamp.com/]]

oilcorruptedspring=[[Corrupted Spring]]
oilcorruptedspring2=[[https://soundcloud.com/oilrama]]
oildreamsunpeeling=[[Dreams unpeeling]]
oildreamsunpeeling2=[[https://soundcloud.com/oilrama]]
oilexcelsior=[[Excelsior!]]
oilexcelsior2=[[https://soundcloud.com/oilrama]]
oilfallingapart=[[Falling apart]]
oilfallingapart2=[[https://soundcloud.com/oilrama]]
oilliminallounge=[[Liminal lounge]]
oilliminallounge2=[[https://soundcloud.com/oilrama]]
oiltheabyss=[[The abyss]]
oiltheabyss2=[[https://soundcloud.com/oilrama]]
oilunraveling=[[Unraveling]]
oilunraveling2=[[https://soundcloud.com/oilrama]]

musicReturnButton = {
	text = "Return",
	x = 500,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	
}


stopbutton = {
	text = "stop",
	x = 100,
	y = 550, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	
}

corruptedspringb = {
	text = "Corrupted Spring",
	x = trackspositionx,
	y = trackspositiony,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

dreamsunpeelingb = {
	text = "Dreams Unpeeling",
	x = trackspositionx,
	y = trackspositiony+25,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

excelsiorb = {
	text = "Excelsior!",
	x = trackspositionx,
	y = trackspositiony+50,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

fallingapartb = {
	text = "Falling apart",
	x = trackspositionx,
	y = trackspositiony+75,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

liminalloungeb = {
	text = "Liminal Lounge",
	x = trackspositionx,
	y = trackspositiony+100,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

theabyssb = {
	text = "The abyss",
	x = trackspositionx,
	y = trackspositiony+125,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

unravelingb = {
	text = "Unraveling",
	x = trackspositionx,
	y = trackspositiony+150,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

end


local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end


local function drawButton (button, hovered)
	
	--love.graphics.setFont( button.font )
	if hovered then
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(button.text,button.x,button.y,button.r,button.sx)
end


function musicdraw()
--print(gamestate)
					love.graphics.setFont(Regular12_0xA000)
					love.graphics.print(oilt,100,100,0,1)
						love.graphics.print(currenttrackname,100,450,0,1)
					--love.graphics.setFont()
					love.graphics.print(oilt2,100,125,0,1)
					love.graphics.print(oilt3,100,150,0,1)
					love.graphics.print(currenttrackdesc,100,475,0,1)
					--love.graphics.print("rate: " .. rate .. " channels: " .. channels,100,800+bcspace,0,1)

	local hovered = isButtonHovered (musicReturnButton)
	drawButton (musicReturnButton, hovered)
		if hovered and love.mouse.isDown(1) then
			love.timer.sleep( 1 )
			gamestate="options"
		end

	local hovered = isButtonHovered (stopbutton)
	drawButton (stopbutton, hovered)
	if hovered and love.mouse.isDown(1) then
	love.audio.stop()
		currenttrackname=""
		currenttrackdesc=""
	love.timer.sleep( 0.3 )
	end
	
	
	local hovered = isButtonHovered (corruptedspringb)
	drawButton (corruptedspringb, hovered)
	if hovered and love.mouse.isDown(1) then
		love.audio.stop()
		currenttrackname=oilcorruptedspring
		currenttrackdesc=oilcorruptedspring2
	love.timer.sleep( 0.3 )
	corruptedspring= love.audio.newSource( "/music/CorruptedSpring.ogg","stream" )
	love.audio.play( corruptedspring )
	end
	
		local hovered = isButtonHovered (dreamsunpeelingb)
	drawButton (dreamsunpeelingb, hovered)
	if hovered and love.mouse.isDown(1) then
		love.audio.stop()
		currenttrackname=oildreamsunpeeling
		currenttrackdesc=oildreamsunpeeling2
	love.timer.sleep( 0.3 )
	dreamsunpeeling= love.audio.newSource( "/music/DreamsUnpeeling.ogg","stream" )
	love.audio.play( dreamsunpeeling )
	end
	
		local hovered = isButtonHovered (excelsiorb)
	drawButton (excelsiorb, hovered)
	if hovered and love.mouse.isDown(1) then
		love.audio.stop()
		currenttrackname=oilexcelsior
		currenttrackdesc=oilexcelsior2
	love.timer.sleep( 0.3 )
	dreamsunpeeling= love.audio.newSource( "/music/Excelsior!.ogg","stream" )
	love.audio.play( dreamsunpeeling )
	end
	
		local hovered = isButtonHovered (fallingapartb)
	drawButton (fallingapartb, hovered)
	if hovered and love.mouse.isDown(1) then
		love.audio.stop()
		currenttrackname=oilfallingapart
		currenttrackdesc=oilfallingapart2
	love.timer.sleep( 0.3 )
	fallingapart= love.audio.newSource( "/music/fallingapart.ogg","stream" )
	love.audio.play( fallingapart )
	end
	
		local hovered = isButtonHovered (liminalloungeb)
	drawButton (liminalloungeb, hovered)
	if hovered and love.mouse.isDown(1) then
		love.audio.stop()
		currenttrackname=oilliminallounge
		currenttrackdesc=oilliminallounge2
	love.timer.sleep( 0.3 )
	liminallounge= love.audio.newSource( "/music/LiminalLounge.ogg","stream" )
	love.audio.play( liminallounge )
	end
	
		local hovered = isButtonHovered (theabyssb)
	drawButton (theabyssb, hovered)
	if hovered and love.mouse.isDown(1) then
		love.audio.stop()
		currenttrackname=oiltheabyss
		currenttrackdesc=oiltheabyss2
	love.timer.sleep( 0.3 )
	theabyss= love.audio.newSource( "/music/TheAbyss.ogg","stream" )
	love.audio.play( theabyss )
	end
	
		local hovered = isButtonHovered (unravelingb)
	drawButton (unravelingb, hovered)
	if hovered and love.mouse.isDown(1) then
		love.audio.stop()
		currenttrackname=oilunraveling
		currenttrackdesc=oilunraveling2
	love.timer.sleep( 0.3 )
	unraveling= love.audio.newSource( "/music/Unraveling.ogg","stream" )
	love.audio.play( unraveling )
	end
		
	end
