
function optionsload()

--levelmap= love.graphics.newImage("/assets/levels/levelselection.png")
sphere= love.graphics.newImage("/assets/boxweiss.png")

		exittext="Exit"  musictext="Music" optionstext="Options" creditst="Credits" returntext="Return" controlstext="controls"

--buttons

returnButton = {
	text = "Return",
	x = 500,
	y = 100, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
	
}

exitButton = {
text = exittext,
	x = 10,
	y = 10, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

musicButton = {
text = musictext,
	x = 10,
	y = 30, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

creditsButton = {
text = creditst,
	x = 10,
	y = 90,
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}

controlsButton = {
text = controlstext,
	x = 10,
	y = 50, 
	r = 0,
	sx = 1,
	hovered = false,
	color = {1,1,1},
	hoveredColor = {1,1,0},
}


end

function levelbutton()
	
end

function levelupdate(dt)
	  
end

local function isButtonHovered (button)
	local font = button.font or love.graphics.getFont( )
	local width = font:getWidth(button.text)
	local height = font:getHeight( )
	local sx, sy = button.sx or 1, button.sy or button.sx or 1
	local x, y = button.x, button.y
	local w, h = width*sx, height*sy
	local mx, my = love.mouse.getPosition()
	if mx >= x and mx <= x+w
		and my >= y and my <= y+h then
		button.w, button.h = w, h
		return true
	end
	return false
end

local function drawButton (button, hovered,text)
	if hovered then
	--love.graphics.setFont( button.font )
	
		love.graphics.setColor(button.hoveredColor)
		love.graphics.rectangle ('line', button.x, button.y, button.w, button.h)
	else
		love.graphics.setColor(button.color)
	end
	love.graphics.print(text,button.x,button.y,button.r,button.sx)
end



function optionsmenudraw()
 	--exit
	local hovered = isButtonHovered (exitButton)
	drawButton (exitButton, hovered,exittext)
	if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.5 )
				love.event.quit()
	end
	
			--Return
		
			local hovered = isButtonHovered (returnButton)
	drawButton (returnButton, hovered,returntext)
	
	if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.5 )
				gamestate="game"
	end
	
		--Music player
		
			local hovered = isButtonHovered (musicButton)
	drawButton (musicButton, hovered,musictext)
	
	if hovered and love.mouse.isDown(1) then 
				love.timer.sleep( 0.5 )
				gamestate="music"
	end
	
				--Help
		
		local hovered = isButtonHovered (controlsButton)
	drawButton (controlsButton, hovered,controlstext)

	if hovered and love.mouse.isDown(1) then
		love.timer.sleep( 0.3 )
			if helpison==false then helpison=true
		elseif helpison==true then helpison=false
		end
	end
	
			--Credits

		local hovered = isButtonHovered (creditsButton)
	drawButton (creditsButton, hovered,creditst)
	if hovered and love.mouse.isDown(1) then 
	love.timer.sleep( 0.3 )
			if creditsison==false then creditsison=true
		elseif creditsison==true  then creditsison=false
		end
	end
	

	if helpison then love.graphics.print(help,100,200,0,left, 0.8, 1) end
	if creditsison then 
	love.graphics.print(creditstext,50,100,0,left, 0.8, 1) 
	love.graphics.print(creditstext2,300,100,0,left, 0.8, 1) 
	love.graphics.print(creditstext3,500,100,0,left, 0.8, 1) 
	end
end
