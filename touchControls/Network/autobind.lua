function testIPRange(startIP, endIP, port)
    print("Start IP:", startIP)
    print("End IP:", endIP)
    print("Port:", port)

    -- Parse start IP
    local startParts = {startIP:match("(%d+)%.(%d+)%.(%d+)%.(%d+)")}
    local start = tonumber(startParts[1]) * 16777216 + tonumber(startParts[2]) * 65536 + tonumber(startParts[3]) * 256 + tonumber(startParts[4])

    -- Parse end IP
    local endParts = {endIP:match("(%d+)%.(%d+)%.(%d+)%.(%d+)")}
    local endNum = tonumber(endParts[1]) * 16777216 + tonumber(endParts[2]) * 65536 + tonumber(endParts[3]) * 256 + tonumber(endParts[4])

    if start == nil or endNum == nil then
        print("Invalid start or end IP provided.")
        invalidIp:play()
        return
    end
    
    print("Start:", start)
    print("End:", endNum)
    print("Port:", port)  -- Ensure that port is correctly received
    
    local ipParts = {tonumber(startParts[1]), tonumber(startParts[2]), tonumber(startParts[3]), tonumber(startParts[4])}
    local instanceResponse = "INSTANCE_RESPONSE"  -- Define the response expected from instances

    while start <= endNum do
        local ip = table.concat(ipParts, ".")
        print("Testing IP:", ip)

        local client = socket.udp()
        client:settimeout(1)  -- Set a timeout of 1 second for receiving responses

        -- Send a message to the IP address and port
        local message = "INSTANCE_CHECK"
        print("Sending message:", message, "to:", ip, "port:", port)
        client:sendto(message, ip, port)

        -- Attempt to receive a response
        local response, rIp, rPort = client:receivefrom()
        if response == instanceResponse then
            print("Received valid response from instance:", ip)
            validInstanceFound:play()
            receiveAddress = ip
            sendAddress = ip
            searchingForInstances=false
            -- Further actions based on the response can be added here
            break  -- Exit the loop if a response is received from an instance
        else
            print("Received response from:", ip, "but it's not a valid instance.")
            noValidInstanceFound:play()
        end

        if ip == endIP then
            print("Reached end of range.")
            searchingForInstances=false
            noValidInstanceFound:play()
            break
        end

        ipParts[4] = ipParts[4] + 1
        if ipParts[4] > 255 then
            ipParts[4] = 0
            ipParts[3] = ipParts[3] + 1
            if ipParts[3] > 255 then
                ipParts[3] = 0
                ipParts[2] = ipParts[2] + 1
                if ipParts[2] > 255 then
                    ipParts[2] = 0
                    ipParts[1] = ipParts[1] + 1
                end
            end
        end

        start = start + 1
    end
end


function listenForMessages(port)
    print("Listening for messages on port:", receivePort)
      
    while true do
        local data, receiveAddress, receivePort = udp:receive()  -- Wait for incoming data
        if data then
            print("Received message:", data, "from:", receiveAddress, "port:", receivePort)
            -- Process the received message and generate a response
            local response = "Message received: " .. data
            print("Sending response:", response)
            -- Send the response back to the sender
            udp:sendto(response, receiveAddress, receivePort)
        else
            --print("Failed to receive message")
        end
           -- Allow Love2D to yield to other operations
        coroutine.yield()
    end
end
