
function touchControls:init()
  -- Create left thumbstick
  --self.leftThumbstickX = love.graphics.getWidth() / 20
  --self.leftThumbstickY = love.graphics.getHeight() / 1.5

  -- Create right thumbstick
  --self.rightThumbstickX = love.graphics.getWidth() * 3 / 4
  --self.rightThumbstickY = love.graphics.getHeight() / 1.5

 -- Create buttons based on layout
  if touchControls.layout == "gamepad" then
    self:createGamepadButtons()
  elseif touchControls.layout == "arcade" then
    self:createArcadeButtons()
  end
end
