-- Add a function to toggle between layouts
function touchControls:toggleLayout()
  if self.layout == "gamepad" then
        self.layout = "arcade"
    elseif self.layout == "arcade" then
        self.layout = "keyboard"
    else
        self.layout = "gamepad"
    end

    -- Recreate buttons based on the new layout
    self.buttons = {}
    if self.layout == "gamepad" then
        self:createGamepadButtons()
    elseif self.layout == "arcade" then
        self:createArcadeButtons()
    elseif self.layout == "keyboard" then
        -- No need to create buttons for the keyboard layout
        self:removeLeftThumbstickButtons()
        self:removeRightThumbstickButtons()
    end
end
