			
			
-- Create buttons for arcade layout
function touchControls:createArcadeButtons()
  -- Clear existing buttons
  self:removeRightThumbstickButtons()
  self.buttons = {}
  -- Create buttons around the left thumbstick
	if MobileOrientation=="portrait" then
			dynamicX =1
			dynamicX2 =1.4
	elseif MobileOrientation=="landscape" then
				dynamicX =0.6
				dynamicX2 =0.8
	end
				
  local buttonPositions = {
    --{x = love.graphics.getWidth() /0.6, y = self.leftThumbstickY * 1},
    --{x = love.graphics.getWidth() /0.8, y = self.leftThumbstickY * 1.05},
     {x = love.graphics.getWidth() / dynamicX, y = self.leftThumbstickY * 1},
     {x = love.graphics.getWidth() / dynamicX2, y = self.leftThumbstickY * 1.05},
    --{x = love.graphics.getWidth() /1, y = self.leftThumbstickY/ 1.7},
    --{x = love.graphics.getWidth() - 200, y = self.leftThumbstickY+100},
  }

  for _, position in ipairs(buttonPositions) do
    table.insert(self.buttons, {x = position.x, y = position.y, pressed = false})
  end
  
  -- Remove any existing buttons associated with the right thumbstick
  --self:removeRightThumbstickButtons()
end

