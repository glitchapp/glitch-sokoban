-- touchControls.lua

require ("touchControls/Menu/Buttons")
require ("touchControls/Menu/DrawButton")

touchControls = {
  leftThumbstickRadius = 440,
  rightThumbstickRadius = 240,
  buttonRadius = 150,
  buttonDistance = 160,
  leftxaxis = 0,
  leftyaxis = 0,
  rightxaxis = 0,
  rightyaxis = 0,
  leftThumbstickPressed = false,
  rightThumbstickPressed = false,
  buttons = {}
}


touchControls.layout="arcade"


-- Enum for layout types
local LayoutType = {
	GAMEPAD = 1,
    ARCADE = 2,
    KEYBOARD = 3, -- Add KEYBOARD layout
}

require ("touchControls/Gamepad/createGamepadButtons")
require ("touchControls/Gamepad/createArcadeButtons")
require ("touchControls/Gamepad/removeLeftThumbstickButtons")
require ("touchControls/Gamepad/removeRightThumbstickButtons")
require ("touchControls/Gamepad/toggleLayout")
require ("touchControls/Gamepad/touchEvents")
require ("touchControls/Gamepad/mouseEvents")
require ("touchControls/Menu/checkMenuButtons")
require ("touchControls/Menu/IsHovered")

touchControls.layout = "arcade"
function touchControls:init()
 --if not (touchControls.layout == "keyboard") then
  -- Create left thumbstick
  self.leftThumbstickX = love.graphics.getWidth() / 3.2
  self.leftThumbstickY = love.graphics.getHeight() / 4.2


  -- Create right thumbstick
  --self.rightThumbstickX = love.graphics.getWidth() * 3 / 4
  --self.rightThumbstickY = love.graphics.getHeight() / 1.8
--end
 -- Create buttons based on layout
  if touchControls.layout == "gamepad" then
    --self:createGamepadButtons()
  elseif touchControls.layout == "arcade" then
    self:createArcadeButtons()
  elseif touchControls.layout == "keyboard" then
  end
	
end




require ("touchControls/Gamepad/updateThumbstickAxes")
require ("touchControls/Gamepad/checkButtonPress")
require ("touchControls/Gamepad/drawTouchControls")
require ("touchControls/Gamepad/adjustButtons")



-- Return touchControls table
return touchControls

