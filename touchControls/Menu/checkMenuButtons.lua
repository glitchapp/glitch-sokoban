  
  -- Add a mousepressed event to handle layout switching
function checkMenuButtons(x, y, button, istouch, presses)
  if button == 1 then -- Left mouse button
    -- Check if the mouse click is within the layout switch button
    local switchButtonX = love.graphics.getWidth() - 100
    local switchButtonY = 20
    local switchButtonWidth = 80
    local switchButtonHeight = 30

    if x >= switchButtonX and x <= switchButtonX + switchButtonWidth and
       y >= switchButtonY and y <= switchButtonY + switchButtonHeight then
      touchControls:toggleLayout()
    end
    
    if touchControls.layout == "keyboard" then
        touchKeyboard:mousereleased(x, y, button, istouch, presses)
    end
    
  end
end
