    maplogic = Object:extend()

    player = '@'
    playerOnStorage = '+'
    box = '$'
    boxOnStorage = '*'
    storage = '.'
    wall = '#'
    empty = ' '
    boxX=0
    boxY=0
    cellSize = 32
    spacex=0
    spacey=0

--Graphics
--l1
--wallpng=love.graphics.newImage("l1wall.png")
--boxpng=love.graphics.newImage("l1box.png")
--strpng=love.graphics.newImage("l1storage.png")
--playeronstrpng=love.graphics.newImage("l1playeronstorage.png")
--boxonstrpng=love.graphics.newImage("l1boxonstorage.png")

--l2
--wallpng=love.graphics.newImage("l2wallblau.png")
--boxpng=love.graphics.newImage("l2box.png")
--strpng=love.graphics.newImage("l2storage.png")
--playeronstrpng=love.graphics.newImage("l2playeronstorage.png")
--boxonstrpng=love.graphics.newImage("12boxonstorage.png")

--wallpng=love.graphics.newImage("l3wall.png")
--boxpng=love.graphics.newImage("l3box.png")
--strpng=love.graphics.newImage("l3storage.png")
--playeronstrpng=love.graphics.newImage("l3playeronstorage.png")
--boxonstrpng=love.graphics.newImage("l3boxonstorage.png")

--[[wallpng=love.graphics.newImage("/assets/wall.png")
boxpng=love.graphics.newImage("/assets/box.png")
playerpng=love.graphics.newImage("/assets/player.png")
playeronstrpng=love.graphics.newImage("/assets/playeronstorage.png")
strpng=love.graphics.newImage("/assets/storage.png")
boxonstrpng=love.graphics.newImage("/assets/boxonstorage.png")
--]]
--floorpng=love.graphics.newImage("floor.png")
--skypng=love.graphics.newImage("sky.png")




    function sleep(s)
        local ntime = os.clock() + s/10
        repeat until os.clock() > ntime
      end
      

    --audiosource = love.audio.newSource("heavy-rain-storm.ogg", "static")





--if joystick:isGamepadDown("dpup","dpdown","dpleft","dpright") then
   -- print("success")
--end
  --require ('gamepadpressed')
            
  --require ('keypressed')           
  
  --require ('touchpressed')
  
  --require ('mousepressed')
  
  --require ('help')
  
    --require ('credits')
  
  
  
  local reflectionIntensity = 0.01
  
       local lightPos2 = {0.1,0.1, 0.15-- Z position (depth) of light
            }
  
--Drawing a level
function maplogic:draw()

--effect(function()
 
--	love.graphics.draw(l800,0,0,0,1,1)
--	love.graphics.setShader(shader)
		if envMap then
			vectorNormalsNoiseShadowEnvmapShader:send("u_envMap", envMap)
		end
  love.graphics.setShader(vectorNormalsNoiseShadowEnvmapShader)
          
  
    for y, row in ipairs(level) do
        for x, cell in ipairs(row) do
            --if cell ~= empty then
                
--set colors
local colors = {
                    [player] = {219,73,73},
                    [playerOnStorage] = {182,146,109},
                    [box] = {29,43,83},
                    [boxOnStorage] = {.59, 1, .5},
                    --[boxOnStorage] = {251, 0, 154},
                    [storage] = {238,202,165},
                    [wall] = {255, 255,255},
                    [empty] = {146, 73, 73},
                }
               
--set tiles
                if cell==player then
                    tilepng=playerpng
                    maplogic.normal=playern
                    maplogic.height=playerh
                    love.graphics.draw(floorpng, (x-spacex) * cellSize,(y-spacey) * cellSize,0,tilesize,tilesize)
                elseif cell==playerOnStorage then
                    tilepng=playeronstrpng
                    maplogic.normal=playeronstrn
                    maplogic.height=playeronstrh
                elseif cell==box then
                    tilepng=boxpng
                    maplogic.normal=boxn
                    maplogic.height=boxh
                    love.graphics.draw(floorpng, (x-spacex) * cellSize,(y-spacey) * cellSize,0,tilesize,tilesize)
                elseif cell==boxOnStorage then
                    tilepng=boxonstrpng
                    maplogic.normal=boxonstrn
                    maplogic.height=boxonstrh
                elseif cell==wall then
                    tilepng=wallpng
                    maplogic.normal=walln
                    maplogic.height=wallh
                elseif cell==storage then
                    tilepng=strpng
                    maplogic.normal=strn
                    maplogic.height=strh
                    love.graphics.draw(floorpng, (x-spacex) * cellSize,(y-spacey) * cellSize,0,tilesize,tilesize)
                    elseif cell==empty then
                    tilepng=floorpng
                    maplogic.normal=floorn
                    maplogic.height=floorh
                end


                cellSize=82 tilesize=0.08
                
                --camera
                if 		cell==player and x<10 and cameraon==true then  spacex=x-7
				elseif  cell==player and x>10 and cameraon==true then  spacex=x-10
                end
                
                if	cell==player and y<7 and cameraon==true then spacey=0
                elseif 	cell==player and y>7 and cameraon==true then  spacey=y-7
                end
                
                love.graphics.setColor(colors[cell])
                          if cell == floor then
							love.graphics.setColor(0,0,0,1)
						end
                
 
        
        -- Set shader uniforms for primary light source
         -- Set shader parameters for normal mapping
         if self.normal then
			vectorNormalsNoiseShadowEnvmapShader:send("u_normals", maplogic.normal)
		end
		if self.height then
			vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", self.height)
		end
		
			
		rainIntensity = 0.1
		red, green, blue = 1,1,1
			-- Set reflection intensity
			vectorNormalsNoiseShadowEnvmapShader:send("ReflectionIntensity", 5 * rainIntensity)
			
		-- Base and maximum reflection intensity values
			local minReflection = 0.5  -- Reflection at night
			local maxReflection = 10.0  -- Reflection at peak sunlight
			local sunIntensity = 1
			-- Dynamically calculate reflection intensity based on sunIntensity
			if cell == wall then
				reflectionIntensity = 0.01
			else
				reflectionIntensity = minReflection + (maxReflection - minReflection) / sunIntensity
			end
			-- Send the dynamically adjusted reflection intensity to the shader
			vectorNormalsNoiseShadowEnvmapShader:send("ReflectionIntensity", reflectionIntensity)

			
		--end
        vectorNormalsNoiseShadowEnvmapShader:send("Resolution", {love.graphics.getWidth(), love.graphics.getHeight()})
        
           -- Set grain intensity
        local grainIntensity = 0.1  -- Adjust as needed
        vectorNormalsNoiseShadowEnvmapShader:send("GrainIntensity", grainIntensity)
       
        -- Calculate the primary light position
        lightPos1 = {0.1 , 0.6 , 0.1}
        vectorNormalsNoiseShadowEnvmapShader:send("LightPos1", lightPos1)
        vectorNormalsNoiseShadowEnvmapShader:send("LightColor1", {0.5, 0.1, 0.1, 10.0})    
        
        vectorNormalsNoiseShadowEnvmapShader:send("AmbientColor", {1,1,1,0.8})    
                    
        vectorNormalsNoiseShadowEnvmapShader:send("Falloff", {0.2, 1.0, 1.0})
		
      
			if cell == player then
        -- Set the second light based on the player position
            lightPos2 = {
               ((x-spacex) * cellSize)/800,  -- X position of light
                (y-spacey) * cellSize / 1000, -- Y position of light
                0.15                                        -- Z position (depth) of light
            }

          end
            --print(lightPos2[2])
            vectorNormalsNoiseShadowEnvmapShader:send("LightPos2", lightPos2)
            vectorNormalsNoiseShadowEnvmapShader:send("LightColor2", {0.0, 1.0, 1.0, 4.0})  -- Light color for dragged car
            
                
                love.graphics.draw(tilepng, (x-spacex) * cellSize,(y-spacey) * cellSize,0,tilesize,tilesize)
                
         --  end
        end
    
    if self.normal then
			vectorNormalsNoiseShadowEnvmapShader:send("u_normals", l800)
		end
		if self.height then
			vectorNormalsNoiseShadowEnvmapShader:send("u_heightmap", l800)
		end
    end
 -- end)
    
  love.graphics.setShader()
  
  love.graphics.print("Level :" .. currentLevel,10,10)	--print currentlevel
        
--if myres=="640x480" then        
--love.graphics.draw(vrgametrigger,720,520,0,0.2)			
--elseif myres=="1920x1080" then
--love.graphics.draw(vrgametrigger,1600,820,0,0.2)
--end

--if selectlevelison then
--love.graphics.draw(selectlevel,500,300,0,0.8)  
--  end

--if touchinterfaceison==true then --and myres=="640x480" then
	--love.graphics.draw(vrgameinterface,500,300,0,0.8)
--elseif  touchinterfaceison==true and myres=="1920x1080" then
--	love.graphics.draw(vrgameinterface,1400,520,0,0.8)
--end

    if myres=="800x600" then love.graphics.setFont(love.graphics.newFont(16)) 
elseif myres=="1920x1080" then love.graphics.setFont(love.graphics.newFont(32))
	end
    
if helpison then love.graphics.print(help,100,400,0,left, 0.8, 1) end
--if currentLevel==1 then love.graphics.print("Press 'O'for options",100,500,0,left, 0.8, 1) end
--love.graphics.setShader()
--print(currentLevel)

end
