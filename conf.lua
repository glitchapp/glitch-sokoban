-- Configuration
function love.conf(t)
	t.title = "Glitch Sokoban" -- The title of the window the game is in (string)
	--t.version = "11.5"         -- The LÖVE version this game was made for (string)
	--t.window.width = 480        -- we want our game to be long and thin.
	--t.window.height = 600
  t.window.width = 1020
  t.window.height = 2340
	t.window.resizable = true
	 t.modules.touch = true
	-- For Windows debugging
	t.console = true
end
