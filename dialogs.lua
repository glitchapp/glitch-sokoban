dialogs= Object:extend(loadLevel)

--dialogs
rainydialog="I find myself in the middle of a storm. I need to act fast!"
rainydialogdone=false
rainydialog2="I'm progressing but I need to keep moving and find refuge from this storm"
rainydialog2done=false
sunnydialog1="This place is so warm... I need to place all these boxes and escape"
sunnydialog1done=false
yellowparticlesdialog1="What is this place? Where is this noise coming from?"
yellowparticlesdialog1done=false
yellowparticlesdialog2="Is this some kind of radio signal?"
yellowparticlesdialog2done=false
redgodslightdialog1="Where am I? It feels like time and space is different here"
redgodslightdialog1done=false
greenleveldone=false
brownlevel60=false

level1done=false
level10done=false
level20done=false
level30done=false
level35done=false
level40done=false
level45done=false
level50done=false


function dialogs:draw()
love.graphics.setFont(love.graphics.newFont(16))
--dialogs first
  if currentLevel==35 then
        love.graphics.print(rainydialog,10,550)
        if rainydialogdone==false then
			--ifindmyself= love.audio.newSource( "dialogs/ifindmyself.ogg","stream" )
			--ifindmyself:setEffect('myEffect')
			--ifindmyself:play()
			
			----thunder= love.audio.newSource( "sounds/--thunder.ogg","stream" )
			----thunder:setVolume(0.3)
			--thunder:play()
			
			--rainloop= love.audio.newSource( "sounds/--rainloop.ogg","stream" )
			--rainloop:setVolume(0.1)
			--rainloop:play()
			
			rainydialogdone=true
		end
  
  elseif currentLevel==38 then
        love.graphics.print(rainydialog2,10,550)
        if rainydialog2done==false then
        --if ifindmyself:isPlaying() then ifindmyself:stop() end
        --improgressing= love.audio.newSource( "dialogs/improgressing.ogg","stream" )
		--improgressing:setEffect('myEffect')
		--improgressing:play()
		
			--if --rainloop:isPlaying() then --rainloop:stop() end
			--thunder:stop()
			--thunder= love.audio.newSource( "sounds/--thunder.ogg","stream" )
			--thunder:setVolume(0.3)
			--thunder:play()
			
			--rainloop= love.audio.newSource( "sounds/--rainloop.ogg","stream" )
			--rainloop:setVolume(0.1)
			--rainloop:play()
		    
		rainydialog2done=true    
		end
  
  elseif currentLevel==40 then
  love.graphics.setColor(0, 0, 0)
        love.graphics.print(sunnydialog1,10,550)
        love.graphics.setColor(1, 1, 1)
        if sunnydialog1done==false then
        --if --rainloop:isPlaying() then --rainloop:stop() end
        --if ifindmyself:isPlaying() then ifindmyself:stop() end
        --if improgressing:isPlaying() then improgressing:stop() end
		--thunder:stop()
		--thisplaceissowarm= love.audio.newSource( "dialogs/thisplaceissowarm.ogg","stream" )
		--thisplaceissowarm:setEffect('myEffect')
		--thisplaceissowarm:play()           
		sunnydialog1done=true
		end
  end
  
  if currentLevel==45 then
        love.graphics.print(yellowparticlesdialog1,10,550)
        if yellowparticlesdialog1done==false then
        --if thisplaceissowarm:isPlaying() then thisplaceissowarm:stop() end
        --whatisthisplace= love.audio.newSource( "dialogs/whatisthisplace.ogg","stream" )
		--whatisthisplace:setEffect('myEffect')
		--whatisthisplace:play()        
		
			electricmodem= love.audio.newSource( "sounds/electricmodem.ogg","stream" )
			electricmodem:setVolume(0.1)
			electricmodem:play()
		    
		yellowparticlesdialog1done=true
		end
  
  elseif currentLevel==47 then
        love.graphics.print(yellowparticlesdialog2,10,550)
        if yellowparticlesdialog2done==false then
        --if whatisthisplace:isPlaying() then whatisthisplace:stop() end
        --isthissomekindof= love.audio.newSource( "/dialogs/isthissomekindof.ogg","stream" )
		--isthissomekindof:setEffect('myEffect')
		--isthissomekindof:play()
		
		rainydialog2done=true    
		
		yellowparticlesdialog2done=true
		end
  
  elseif currentLevel==50 then
   love.graphics.print(redgodslightdialog1,10,550)
		if redgodslightdialog1done==false then
			if electricmodem:isPlaying() then electricmodem:stop() end  --if isthissomekindof:isPlaying() then isthissomekindof:stop() end
			--whereami= love.audio.newSource( "/dialogs/whereami.ogg","stream" )
			--whereami:setEffect('myEffect')
			--whereami:play()
					
				paranoia1= love.audio.newSource( "sounds/PM_PARANOIA_DRONE_12.ogg","stream" )
				paranoia1:setEffect('myEffect')
				paranoia1:setVolume(1)
				paranoia1:play()
		
			redgodslightdialog1done=true
			
		end
  
    elseif currentLevel==55 then
   
        if greenleveldone==false then
			if paranoia1:isPlaying() then paranoia1:stop() end
					
			ghost1= love.audio.newSource( "sounds/GHOSTSAtmosphere.ogg","stream" )
			ghost1:setVolume(1)
			ghost1:play()
		
			greenleveldone=true
		end
  
  
      elseif currentLevel==60 then
   
        if brownlevel60==false then
        if ghost1:isPlaying() then ghost1:stop() end
		
		brownlevel60=true
		end
  end
  
  end
