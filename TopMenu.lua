-- Define variables to keep track of swipe gesture and selected option

local startX, startY = 0, 0
local deltaX, deltaY = 0, 0
isSwiping = false
 menuVisible = false
selectedOption = nil
threshold = 50 -- Threshold to switch between options
local options = {"Option 1", "Option 2", "Option 3", "Option 4", "Option 5", "Option 6", "Option 7"}

-- Variables for scrolling
local scrollOffsetX, scrollOffsetY = 100, 0
local lastMouseX, lastMouseY = 0, 0
--local mouseHeld = false
hasSwiped = false -- Flag to indicate if a swipe has occurred

		Mono64_0xA000_256 = love.graphics.newFont("fonts/0xa000/0xA000-Mono.ttf", 256)
		Mono64_0xA000 = love.graphics.newFont("fonts/0xa000/0xA000-Mono.ttf", 64)
		Regular12_0xA000 = love.graphics.newFont("fonts/0xa000/0xA000-Regular.ttf", 12)
		
		GUI_Sound_Effects_027 = love.audio.newSource("sounds/GUI/GUI_Sound_Effects/GUI_Sound_Effects_027.ogg", "static")
		GUI_Sound_Effects_071 = love.audio.newSource("sounds/GUI/GUI_Sound_Effects/GUISound_Effects_8_BY_Jalastram/GUI_Sound_Effects_071.ogg", "static")
		GUI_Sound_Effects_072 = love.audio.newSource("sounds/GUI/GUI_Sound_Effects/GUISound_Effects_8_BY_Jalastram/GUI Sound Effects_072.ogg", "static")
	

function TopMenuMousepressed(x, y, button, istouch, presses)
	
    mouseHeld = true
    startY = y
    isSwiping = true
    scrollOffsetY = y
    hasSwiped = false -- Reset swipe flag on mouse press
    
    --print(mouseHeld)
end

function TopMenuMousereleased(x, y, button, istouch, presses)

    mouseHeld = false
    isSwiping = false
    menuVisible = false
    
    
    lastMouseX = 0
    lastMouseY = 0
    if not hasSwiped then
        selectedOption = nil
    end
    deltaX = 0
    deltaY = 0
    
    -- Perform action based on selected option
    if selectedOption == options[1] then 					-- Title Play Map
			
			loadLevel()
            GUI_Sound_Effects_072:play()
         print("Option 1 selected: Restart")   
    elseif selectedOption == options[2] then 				-- Performance mode
			
		        if gamestate=="game" or gamestate=="music" then 		gamestate="preview"
			elseif gamestate=="preview" then	gamestate="game"
			end
			
			love.timer.sleep( 0.1 )
        GUI_Sound_Effects_071:play()
        print("Option 2 selected: Show all levels")   
        
    elseif selectedOption == options[3] then 				-- FPS Graph
				if gamestate=="game" or gamestate=="preview" then 		gamestate="music"
			elseif gamestate=="music" then		gamestate="game"
			end

			print("Option 3 selected: music")   
    elseif selectedOption == options[4] then 				-- Tutorial
        -- Add your action for Option 4 here
			print("Option 4 selected: Exit")   
			love.event.quit()

       --[[
    print("Option 4 selected: Tutorial")   
    
    elseif selectedOption == options[5] then 				-- Credits
        -- Add your action for Option 5 here
        title.activemenu = title.creditsmenu
        mode = "title"
		GUI_Sound_Effects_071:play()
    print("Option 5 selected: Credits")   
    
    elseif selectedOption == options[6] then 
        -- Add your action for Option 6 here
    print("Option 7 selected: Network controls")   
			if NetworkMenuVisible==false then NetworkMenuVisible=true
		elseif NetworkMenuVisible==true then NetworkMenuVisible=false
		end
		
    elseif selectedOption == options[7] then 
        -- Add your action for Option 6 here
    print("Option 6 selected: Exit")   
    love.event.quit()
    --]]
    elseif selectedOption == nil then 
        print("No options selected")
    end

end



function TopMenuMousemoved(x, y, dx, dy, istouch)
    startX = startX or 0
    startY = startY or 0

    if mouseHeld == true then
        deltaX = x - startX
        deltaY = y - startY

        if scrollOffsetX < 600 then
            scrollOffsetX = scrollOffsetX + dx
        end

        local delta = deltaY or deltaX

        for i = 4, 1, -1 do  -- Iterate from 3 to 1 in reverse order
            local thresholdLow = 200 - (3 - i + 1) * 75
            local thresholdHigh = 400 - (3 - i) * 75

            if delta >= thresholdLow and delta < thresholdHigh then
                hasSwiped = true
                selectedOption = options[i]
                menuVisible = true
                GUI_Sound_Effects_027:play()

                -- Debug statement to show selected option and delta range
                print("Selected Option: " .. selectedOption .. " | Delta Range: " .. thresholdLow .. " to " .. thresholdHigh)

                return
            end
        end

        selectedOption = nil
        menuVisible = false
        scrollOffsetX = 0
        scrollOffsetY = 0

        -- Debug statement if no option is selected
        print("No Option Selected")
    end
end



function revealMenu()

    
    
     width, height = love.graphics.getDimensions( )
    local optionHeight = 350
    local optionSpacing = 50
    local menuWidth = width *3
    
		 --local baseY = 0
		 

         baseY = (optionHeight - optionSpacing / 2) - scrollOffsetX - 0
        
    
    
     local transparency = 0.7
    love.graphics.setColor(0, 0, 1, transparency) -- Option 1 color (blue)
    love.graphics.rectangle("fill", -200, baseY, menuWidth, optionHeight)
    
    love.graphics.setColor(1, 0, 0, transparency) -- Option 2 color (red)
    love.graphics.rectangle("fill", -200, baseY - optionSpacing, menuWidth, optionHeight)
    
    love.graphics.setColor(0, 1, 0, transparency) -- Option 3 color (green)
    love.graphics.rectangle("fill", -200, baseY - 2 * optionSpacing, menuWidth, optionHeight)
  
    love.graphics.setColor(0, 1, 1, transparency) -- Option 4 color (yellow)
    love.graphics.rectangle("fill", -200, baseY - 3 * optionSpacing, menuWidth, optionHeight)
  --[[    
    love.graphics.setColor(0.5, 0.5, 1, transparency) -- Option 5 color
    love.graphics.rectangle("fill", 0, baseY - 4 * optionSpacing, menuWidth, optionHeight)
    
    love.graphics.setColor(0.5, 1, 0.5, transparency) -- Option 6 color
    love.graphics.rectangle("fill", 0, baseY - 5 * optionSpacing, menuWidth, optionHeight)
    
    love.graphics.setColor(1, 0.5, 1, transparency) -- Option 7 color
    love.graphics.rectangle("fill", 0, baseY - 5 * optionSpacing, menuWidth, optionHeight)
    --]]
    love.graphics.setFont(Mono64_0xA000)
    
    for i, option in ipairs(options) do
    
        local optionY = baseY - (i - 1) * optionSpacing
        
        
        
        if selectedOption == option then
            --love.graphics.setColor(0, 1, 0, 1) -- Highlight color (green, semi-transparent)
            local transparency = 1
            love.graphics.rectangle("fill", -200, optionY, menuWidth, optionHeight)
            love.graphics.setColor(1, 1, 1, 1) -- Text color (white)
            
            -- Adjust text based on the option
            local text = ""
				if option == "Option 1" then
					text = "Restart"
            elseif option == "Option 2" then
                text = "Show levels"
            elseif option == "Option 3" then
                text = "Music"
            elseif option == "Option 4" then
                text = "Exit"
            end
            
            love.graphics.printf(text, width / 2 , optionY, 1000, "center", 0, 1)
        end
    end
  
    --love.graphics.setFont(Regular12_0xA000)
end


function hideMenu()

    menuVisible = false
    isSwiping = false

end
