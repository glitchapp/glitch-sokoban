function loadassets()
require 'levels'

 --assets
  wallpng=love.graphics.newImage("/assets/wall.png")
  --wallpng=love.graphics.newImage("/assets/old/wall.png")
boxpng=love.graphics.newImage("/assets/box.png")
playerpng=love.graphics.newImage("/assets/player.png")
playeronstrpng=love.graphics.newImage("/assets/playeronstorage.png")
strpng=love.graphics.newImage("/assets/storage.png")
--strpng=love.graphics.newImage("/assets/old/storage.png")
boxonstrpng=love.graphics.newImage("/assets/boxonstorage.png")
floorpng=love.graphics.newImage("/assets/floor.png")

--normals
walln=love.graphics.newImage("/assets/normals/wall.png")
--walln=love.graphics.newImage("/assets/old/normals/wall.png")
boxn=love.graphics.newImage("/assets/normals/box.png")
playern=love.graphics.newImage("/assets/normals/player.png")
playeronstrn=love.graphics.newImage("/assets/normals/playeronstorage.png")
strn=love.graphics.newImage("/assets/normals/storage.png")
--strn=love.graphics.newImage("/assets/old/normals/storage.png")
boxonstrn=love.graphics.newImage("/assets/normals/boxonstorage.png")
floorn=love.graphics.newImage("/assets/normals/floor.png")

--height maps
wallh=love.graphics.newImage("/assets/height/wall.png")
--wallh=love.graphics.newImage("/assets/old/height/wall.png")
boxh=love.graphics.newImage("/assets/height/box.png")
playerh=love.graphics.newImage("/assets/height/player.png")
playeronstrh=love.graphics.newImage("/assets/height/playeronstorage.png")
strh=love.graphics.newImage("/assets/height/storage.png")
--strh=love.graphics.newImage("/assets/old/height/storage.png")
boxonstrh=love.graphics.newImage("/assets/height/boxonstorage.png")
floorh=love.graphics.newImage("/assets/height/floor.png")

 envMap = love.graphics.newCubeImage({
    "assets/env/posx.jpg", "assets/env/negx.jpg", "assets/env/posy.jpg", "assets/env/negy.jpg", "assets/env/posz.jpg", "assets/env/negz.jpg"
	})

  shaderChannel={
  --[0]=love.graphics.newImage("channel/tex10.png"),
  --[1]=love.graphics.newImage("channel/tex14.png"),
}
  
  vrgameinterface= love.graphics.newImage("/assets/touchcontrols3.png")
vrgametrigger= love.graphics.newImage("/assets/touchcontrols.png")
  l800= love.graphics.newImage("/assets/l800.png")
  l1920= love.graphics.newImage("/assets/l1920.png")

end

function loadassets2()
require 'gsok7/levels'
--assets
  wallpng=love.graphics.newImage("gsok7/assets/wall.png")
boxpng=love.graphics.newImage("gsok7/assets/box.png")
playerpng=love.graphics.newImage("gsok7/assets/player.png")
playeronstrpng=love.graphics.newImage("gsok7/assets/playeronstorage.png")
strpng=love.graphics.newImage("gsok7/assets/storage.png")
boxonstrpng=love.graphics.newImage("gsok7/assets/boxonstorage.png")
 
  shaderChannel={
  [0]=love.graphics.newImage("gsok7/channel/tex10.png"),
  --[1]=love.graphics.newImage("gsok7/channel/tex14.png"),
}
  
  vrgameinterface= love.graphics.newImage("gsok7/assets/touchcontrols3.png")
vrgametrigger= love.graphics.newImage("gsok7/assets/touchcontrols.png")
  l800= love.graphics.newImage("gsok7/assets/l800.png")
  l1920= love.graphics.newImage("gsok7/assets/l1920.png")
end
