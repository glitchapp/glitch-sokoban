shadertoy1= Object:extend(loadLevel)
--shadertoy code implement
--modify shaderFileName to the code file you copied.
--if the shader has any channel file, for example channel0 then shaderChannel[1]=love.graphics.newImage(path to the file)
local resolution={800,600,1} --low resolution for better performance
local files = love.filesystem.getDirectoryItems("shader")
local currentShader = 0
local shaderFileName="shader/"..files[currentShader+1]
--[[local shaderChannel={
  [0]=love.graphics.newImage("channel/tex10.png"),
  [1]=love.graphics.newImage("channel/tex14.png"),
  --[3]=love.graphics.newImage("channel/tex15.png"),
}
--]]
local function loadShader(path)
    local iSystem={}
    local header="\n"
    local ender=[[
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords){ 
vec2 fragCoord = texture_coords * iResolution.xy; 
mainImage( color, fragCoord ); 
return color; 
}]]
    local file = love.filesystem.newFile(path)
    file:open("r")
    local shaderData = file:read()
    shaderData = string.gsub(shaderData,"texture2D","Texel")
	shaderData = string.gsub(shaderData,"iTime","iGlobalTime")
	shaderData = string.gsub(shaderData,"precision highp float;","")
    if string.find(shaderData,"iGlobalTime") then
      iSystem.iGlobalTime=0
	  if not string.find(shaderData,"number iGlobalTime") then
		header="\nextern number iGlobalTime;"..header
	  end
    end
    if string.find(shaderData,"iChannel") then
      iSystem.iChannel={}
	  if not string.find(shaderData,"Image iChannel") then
		for k,v in pairs(shaderChannel) do
			header="\nextern Image iChannel"..k..";"..header
		end
	  end
    end
    if string.find(shaderData,"iMouse") then
      iSystem.iMouse = {0,0,-1,-1}
	  if not string.find(shaderData,"vec4 iMouse") then
		header="\nextern vec4 iMouse;"..header
	  end
    end
    if string.find(shaderData,"iResolution") then
      iSystem.iResolution = {resolution[1],resolution[2],1}
	  if not string.find(shaderData,"vec3 iResolution") then
		header="\nextern vec3 iResolution;"..header
	  end
    end
	shaderData=header..shaderData
	if not string.find(shaderData,"vec4 effect") then
		shaderData=shaderData.."\n"..ender
	end
	
	return shaderData,iSystem
end
local shaderData
local iSystem
local shader
local canvas

function nextShader(i)
	currentShader = (currentShader+i)%#files
	shaderFileName="shader/"..files[currentShader+1]
	setShader(i)
end

function setShader(i)
	i = i or 1
	shaderData,iSystem = loadShader(shaderFileName)
	--shaderData,iSystem = loadShader("shader/balls.glsl")
	local OK
	OK, shader = pcall(love.graphics.newShader,shaderData)
	if not OK then
		--[[--
		print("-------SHADER---------")
		print(shaderFileName)
		print("--------ERROR---------")
		print(shader)
		print("------------------------------------")
		--]]--
		nextShader(i)
		setShader(i)
	end
	canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	if iSystem.iResolution then
		shader:send("iResolution",iSystem.iResolution)
	end
	if iSystem.iChannel then
		for k,v in pairs(shaderChannel) do
			shader:send("iChannel"..k,v)
		end
	end
end

setShader(1)

function selectshadertoy(select)
i = i or 1
	--shaderData,iSystem = loadShader(shaderFileName)
	shaderData,iSystem = loadShader(select)
	local OK
	OK, shader = pcall(love.graphics.newShader,shaderData)
	if not OK then
		--[[--
		print("-------SHADER---------")
		print(shaderFileName)
		print("--------ERROR---------")
		print(shader)
		print("------------------------------------")
		--]]--
		nextShader(i)
		setShader(i)
	end
	canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	if iSystem.iResolution then
		shader:send("iResolution",iSystem.iResolution)
	end
	if iSystem.iChannel then
		for k,v in pairs(shaderChannel) do
			shader:send("iChannel"..k,v)
		end
	end
end

--function love.keypressed(key)
--	if key == "right" then
		--nextShader(1)
	--elseif key == "left" then
--		nextShader(-1)
--	end
	--if key == 's' then 
--		selectshadertoy("shader/balls.glsl") 
	--end
--end

function love.resize(w,h)
	if iSystem.iResolution then
		resolution={love.graphics.getWidth(),love.graphics.getHeight(),1}
		iSystem.iResolution = {resolution[1],resolution[2],1}
		shader:send("iResolution",iSystem.iResolution)
		canvas = love.graphics.newCanvas(resolution[1],resolution[2])
	end
end

function love.update(dt)
  if iSystem.iGlobalTime then
    iSystem.iGlobalTime=iSystem.iGlobalTime+dt
    shader:send("iGlobalTime", iSystem.iGlobalTime)
  end
  if iSystem.iMouse then
    iSystem.iMouse[1],iSystem.iMouse[2] = love.mouse.getPosition()
	iSystem.iMouse[1],iSystem.iMouse[2] = resolution[1]-iSystem.iMouse[1],resolution[2]-iSystem.iMouse[2]
    if love.mouse.isDown(1) then
        if iSystem.iMouse[3]<0 then
          iSystem.iMouse[3],iSystem.iMouse[4] = love.mouse.getPosition()
        end
        shader:send("iMouse", iSystem.iMouse)
    else
        iSystem.iMouse[3],iSystem.iMouse[4] = -1, -1
    end
  end
  love.window.setTitle(shaderFileName.."; FPS: "..love.timer.getFPS())
    love.graphics.setShader(shader)
    love.graphics.draw(canvas)
end

--function love.draw()
 --love.graphics.draw(canvas,0,0,math.pi,1,1,resolution[1],resolution[2])
--end
